#!/bin/sh

HERE="$(dirname "$(readlink -f "$0")")"

set -ex

exec podman run --rm -it \
    --name wine \
    -v "$HERE/prefix64:/home/localuser/prefix64:rw" \
    -v "$HERE/Games:/Games:rw" \
    --userns=keep-id:uid=2020,gid=2020 -e DISPLAY -v "$HOME/.Xauthority:/home/localuser/.Xauthority:Z" -v /tmp/.X11-unix:/tmp/.X11-unix \
    --cpus 12 --memory 32g --net=none \
    --device /dev/snd \
    --ipc=host \
    localhost/jenni/wine:latest bash

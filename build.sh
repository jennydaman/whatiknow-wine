#!/bin/sh

HERE="$(dirname "$(readlink -f "$0")")"

set -ex

podman build -t localhost/jenni/wine:latest - < "$HERE/Dockerfile"

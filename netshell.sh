#!/bin/sh

HERE="$(dirname "$(readlink -f "$0")")"

set -ex

exec podman run --rm -it \
    --name wine-shell \
    -v "$HERE/prefix64:/home/localuser/prefix64:rw" \
    --userns=keep-id:uid=2020,gid=2020 -e DISPLAY -v "$HOME/.Xauthority:/home/localuser/.Xauthority:Z" -v /tmp/.X11-unix:/tmp/.X11-unix \
    --cpus 4 --memory 8g \
    --device /dev/snd \
    localhost/jenni/wine:latest bash

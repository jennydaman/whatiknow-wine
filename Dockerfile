# Just wine64 in docker, based on https://github.com/FNNDSC/hplocons-wine

FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

# install wine dependencies
RUN dpkg --add-architecture i386 \
    && apt-get update \
    && apt-get -y upgrade \
    && apt-get -y install python3 xvfb x11vnc xdotool wget tar supervisor net-tools fluxbox gnupg2 xz-utils cabextract

# Install graphics stuff
RUN apt-get install -y libgl1-mesa-glx libvulkan1

# Install custom wine with proton experimental improvements
RUN wget -qO- https://github.com/GloriousEggroll/wine-ge-custom/releases/download/GE-Proton8-25/wine-lutris-GE-Proton8-25-x86_64.tar.xz \
    | tar xvJ --directory=/usr/local

# Alternatively, install the official wine
# RUN mkdir -pm755 /etc/apt/keyrings \
#     && wget -O /etc/apt/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key \
#     && wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources \
#     && apt-get update \
#     && apt-get install --install-recommends -y winehq-stable

# Install winetricks
RUN wget -O /usr/local/bin/winetricks https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks \
    && chmod +x /usr/local/bin/winetricks

RUN useradd -lmU -s /bin/bash -u 2020 localuser
USER localuser

ENV WINEPREFIX=/home/localuser/prefix64 \
    WINEARCH=win64

# RUN wineboot --init \
#     && winetricks -q dotnet462 corefonts

# What I Know about Wine

The work to run a Windows program on Linux using Wine can be divided into six layers:

1. (Optional) Kernel tweaks
2. (Optional) Sandboxing
3. Wine dependencies
4. The wine program itself
5. The wine prefix and fixes
6. The program you want to run itself

The Steam Desk and Steam + Proton is an all-in-one solution for all of these concerns.
But what if you don't want to use Steam (for privacy reasons)?
Generally, manual methods are complex and error-prone.

## A Certain Anime Game

Recently, it has been very easy to play a certain anime game about a traveler and an unknown god.
Over the years, this has been made possible thanks to Value's contributions to Wine, some Rustaceans,
as well as changes to the game's anticheat itself. Contact me for more information.

## Managers

This page describes how to do manually what a "manager" would do automatically.
The benefits to manual usage are increased privacy, customization, and understanding of your system.

The most popular managers, in descending order of popularity, are:

1. https://usebottles.com/
2. https://lutris.net/
3. https://www.playonlinux.com/

## 1. Optional Kernel Tweaks

It's possible to customize the Linux kernel to optimize it for gaming, though I doubt you can get more than 1-10 additional FPS from it. Here are some examples:

- https://github.com/Frogging-Family/linux-tkg
- https://wiki.archlinux.org/title/Unofficial_user_repositories/Repo-ck

## 2. Not-so Optional Sandboxing

Wine does not protect your system from programs, so it is encouraged to use a security sandbox for system isolation of some sorts. Choose one of:

- Podman, easiest for people who already know how to use Docker/Podman from their day jobs.
- [Firejail](https://wiki.archlinux.org/title/Firejail) is somewhat easy.

See also: https://wiki.archlinux.org/title/Wine#Running_Wine_under_a_separate_user_account

## 3. Wine Dependencies

Wine has a bunch of annoying x32 dependencies.

Sandboxing, dependencies, and running Wine as an underprivileged user can all be achieved
using the [Dockerfile](./Dockerfile) in this repository.

## 4. The Wine Program Itself

For gaming, use https://github.com/GloriousEggroll/wine-ge-custom which includes improvements
from Proton experimental.

For general desktop applications, you should just use the normal wine.

## 5. Wine Prefix and Fixes

The best practice is to have a separate Wine prefix for each program you want to run, since
every program is highly sensitive to what libraries and DLLs are installed.

For gaming, you typically need to install:

- https://github.com/doitsujin/dxvk
- Or maybe https://github.com/HansKristian-Work/vkd3d-proton
